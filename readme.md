# New Laku BGT

version 2.0.0

Laku BGT yang baru menggunakan 2 framework, yaitu Laravel sebagai web servis dan Angularjs sebagai <i>Front end</i>. Dokumentasi AngularJS bisa dilihat [disini](https://docs.angularjs.org/api) dan dokumentasi Laravel bisa dilihat [disini](http://laravel.com/docs/4.2/introduction).

Daftar komponen framework yang digunakan dalam Laku BGT

1. Laravel
2. AngularJS
3. Foundation
4. UnderscoreJS
5. jQuery

## Memulai

Ikuti perintah dibawah ini.

```
git clone git@bitbucket.org:marketbiz/lakubgt_v2.git lakubgt_v2
cd lakubgt_v2
composer install
npm install
sudo npm install (linux)
bower install
grunt
```

## Konfigurasi Basis Data

Konfigurasi basis data lokal, pastikan berada dalam folder "lakubgt_v2"

```
cp app/config/local app/config/your-local
vim app/config/your-local/database.php
```

Ganti nama basis data, username, password, dan prefix sesuai dengan konfigurasi di komputer lokal

```
'mysql' => array(
	'driver'    => 'mysql',
	'host'      => 'localhost',
	'database'  => 'your-database',
	'username'  => 'your-username',
	'password'  => 'your-password',
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => 'your-local',
),
```

Tambahkan environment komputer lokal ke dalam bootstrap

```
vim bootstrap start.php
```

Kemudian cari baris berikut

```
$env = $app->detectEnvironment(array(
	'local' => array('marketbizdev'),
	'laptop' => array('yudhi'),
	'production' => array('lakubgt.com')
));
```

Tambahkan baris berikut di bawah 'laptop' => array('yudhi').

```
'your-local' => array('your-computer-name'),
```

## Sistem Laku BGT

Sistem Laku BGT yang baru memisahkan data penjual dan membuat tabel baru untuk setiap penjual. Berikut daftar tabel yang akan dipasang ketika penjual mendaftar.

### Daftar Tabel
1. site_options
2. product
3. product_metas
4. category
5. category_relation
6. orders
7. customers
8. page
9. page_meta

#### Site Options

Tabel site options berisi tentang 

1. storename
2. admin_email
3. tel
4. address
5. city
6. seo_title
7. seo_desc
8. seo_keywords
9. products_per_page
10. template
11. currency
12. show_product
13. upload_folder
14. logo