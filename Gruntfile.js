module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            dist: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {                        
                    'public/assets/css/main.css': 'sass/main.scss'     
                }
            }
        },
        watch: {
            css: {
                files: 'sass/*.scss',
                tasks: ['sass'],
                options: {
                    livereload: true,
                }
            },
            js: {
                files: 'lib/js/main.js',
                tasks: ['uglify'],
                options: {
                    livereload: true
                }
            }
        },
		uglify: {
			build: {
				src: 'lib/js/main.js',
				dest: 'lib/js/main.min.js'
			}
		}
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['sass']);
	grunt.registerTask('dev', ['watch']);
}