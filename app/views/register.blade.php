@extends ('register.register')

@section('main')

<div class="container" ng-controller="RegisterCtrl">
	<div class="row">
		<div class="medium-8 medium-offset-2 columns">
			<div class="box-register">
				<h1>Ayo buat tokomu sekarang!</h1>

				<form action="#" id="form-register" ng-submit="doRegister()">
					<div class="row">
						<div class="large-12 columns">
							<label>Nama
								<input type="text" name="name" placeholder="Nama Lengkap" required />
							</label>
						</div>
					</div><!-- .row -->
					<div class="row">
						<div class="large-12 columns">
							<label>Telepon
								<input type="text" name="tel" placeholder="Telepon" required />
							</label>
						</div>
					</div><!-- .row -->
					<div class="row">
						<div class="large-12 columns">
							<label>Alamat
								<input type="text" name="address" placeholder="Alamat" required />
							</label>
						</div>
					</div><!-- .row -->
					<div class="row">
						<div class="large-12 columns">
							<label>Kota
								<input type="text" name="city" placeholder="Kota" required />
							</label>
						</div>
					</div><!-- .row -->
					<input type="hidden" name="storename" value="<% $storename %>" />
					<input type="hidden" name="email" value="<% $email %>">
					<input type="hidden" name="password" value="<% $password %>">
					<button class="button small">Daftar</button>
				</form>
			</div>
		</div><!-- .columns -->
	</div><!-- .row -->
</div><!-- .container -->

@stop