<!-- Stored in app/views/register/register.blade.php -->
<!doctype html>
<html ng-app="lakubgt" data-framework="angularjs">
<head>
	<!-- include head -->
	@include('layouts.head')

</head>
<body>
	<!-- include header -->
	
	<!-- extend main content -->
	@yield('main')

	<!-- include footer -->
	@include('layouts.footer')
</body>
</html>