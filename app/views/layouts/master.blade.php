<!-- Stored in app/views/layouts/master.blade.php -->
<!doctype html>
<html ng-app="lakubgt" data-framework="angularjs">
<head>
	<!-- include head -->
	@include('layouts.head')

</head>
<body>
	<!-- include header -->
	@include('layouts.header')
	
	<!-- extend main content -->
	@yield('main')

	@include('layouts.footer')
</body>
</html>