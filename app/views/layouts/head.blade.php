<meta charset="UTF-8">
<title><% $page_title %></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<% $url %>/assets/css/main.css" />
<link rel="shortcut icon" type="image/x-icon" href="<% $url %>/assets/img/favicon.ico">
