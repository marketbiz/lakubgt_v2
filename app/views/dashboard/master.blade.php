<!-- Stored in app/views/dashboard/master.blade.php -->
<!doctype html>
<html>
<head>
	<!-- include head -->
	@include('layouts.head')

</head>
<body ng-controller="DashboardCtrl">
	<!-- include header -->
	@include('dashboard.top-bar')

	<!-- include sidebar -->
	@include('dashboard.side-menu')

	<!-- extend main content -->
	@yield('main')

	<!-- include footer -->
	@include('layouts.footer')
</body>
</html>