<!-- Side Menu -->
	<aside class="side-menu">
		<div class="menu-back">&nbsp;</div>
		<nav>
			<img src="<% $url %>/assets/img/logo.png" alt="Laku BGT" class="main-logo" />
			<ul>
				<li ng-class="{ actived: menu==1 }"><a href="#/" ng-click="menu=1">Dasbor</a></li>
				<li ng-class="{ actived: menu==2 }"><a href="#/product" ng-click="menu=2">Produk</a></li>
				<li ng-class="{ actived: menu==3 }"><a href="#/category" ng-click="menu=3">Kategori</a></li>
				<li ng-class="{ actived: menu==4 }"><a href="#/order" ng-click="menu=4">Order</a></li>
				<li ng-class="{ actived: menu==5 }"><a href="#/laman" ng-click="menu=5">Laman</a></li>
				<li ng-class="{ actived: menu==6 }"><a href="#/menu" ng-click="menu=6">Menu</a></li>
				<li ng-class="{ actived: menu==7 }"><a href="#/profil" ng-click="menu=7">Profil</a></li>
				<li ng-class="{ actived: menu==8 }"><a href="#/settings" ng-click="menu=8">Pengaturan Toko</a></li>
			</ul>
		</nav>
	</aside><!-- aside -->
