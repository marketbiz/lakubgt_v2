<!-- Stored in app/views/login.blade.php -->
<!doctype html>
<html>
<head>
	<!-- include head -->
	@include('layouts.head')

</head>
<body class="login-page">
	<div class="" ng-controller="LoginCtrl">
		<div class="login-box">
			<section class="medium-4 columns">
				<img src="<% $url %>/assets/img/logo.png" alt="Laku BGT" class="main-logo" />
			</section>
			<section class="medium-8 columns end">
				<form ng-submit="doLogin()" id="login-form">
					<label for="">Email
						<input type="email" class="email" name="email" placeholder="Email" autofocus required />
					</label> 
					<label for="">Password
						<input type="password" class="password" name="password" placeholder="Password" required />
					</label>
					<button class="button small">Log In</button>

					<br /><a href="#">Forgot your password?</a>
					<div data-alert class="alert-box alert radius ng-hide" ng-hide="alert">
					  Login failed, please check your email and password
					  <a href="#" class="close" ng-click="closeAlert()">&times;</a>
					</div>
				</form>
			</section>
		</div><!-- .row -->
	</div><!-- .container -->
	@include('layouts.footer')
</body>
</html>
