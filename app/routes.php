<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return App::environment();
});

// Register
Route::get('register', 'RegisterController@index');

Route::get('send-email', 'BaseController@sendMail');

// Route::get('site_info', 'BaseController@getStoreBasicInfo');

// Route::get('blug', function() {

// 	$path = public_path('themes');

// 	$files = scandir($path);

// 	foreach ($files as $key => $file) {
// 		if ($file != '.' && $file != '..'):
// 			// print_r($file);
// 			$extract_info = file_get_contents($path . '/' . $file . '/info.json');
// 			// print_r(json_decode($extract_info));
// 		endif;
// 	}

// 	// echo Theme::init('default-17');
// 	// return $files;
// });

// Login
Route::get('login', array( 'before' => 'check.islogin', 'uses' => 'DashboardController@showLogin'));
Route::get('error-404', function() {
	return View::make('error-404');
});

Route::group(array('prefix' => 'site/{store_name}', 'before' => 'check.store'), function() {
	Route::get('/', 'FrontendController@showIndex');

	// Route::get('/', function($store_name) {
	// 	echo $store_name;
	// });

	// Route::get();

});

// Dashboard
Route::group(array('prefix' => 'dashboard', 'before' => 'check.login'), function() {

	// Dashboard
	Route::get('/', 'DashboardController@index');
	Route::get('products', 'ProductController@showProduct');
	Route::get('add-product', 'ProductController@addProduct');
	Route::get('categories', 'ProductController@showCategory');

	Route::get('orders', 'OrderController@showIndex');
	// Route::get();
});

// API Services
Route::group(array('prefix' => 'api/v1'), function()
{
	// Base Info
	Route::get('base-info', 'BaseController@getBaseInfo');

	// User Controller
	Route::post('validate-user', 'UserController@validateUser');
	Route::get('user-actived', 'UserController@getUserActived');
	Route::get('logout', 'UserController@doLogout');

	// Store Controller
	Route::resource('site', 'SiteController');
	Route::get('get-store', 'SiteController@getStore');

	// Register Controller
	Route::post('register', 'RegisterController@availabilityName');
	Route::post('post-new-store', 'RegisterController@postNewStore');

	// Product Controller
	Route::get('get-products', 'ProductController@index');
	Route::get('get-product/{productId}', 'ProductController@show');
	Route::post('post-new-product', 'ProductController@store');
	Route::post('put-product', 'ProductController@edit');
	Route::get('delete-product/{productId}', 'ProductController@destroy');
});


// Route::get('auth', 'Tappleby\AuthToken\AuthTokenController@index');
// Route::post('auth', 'Tappleby\AuthToken\AuthTokenController@store');
// Route::delete('auth', 'Tappleby\AuthToken\AuthTokenController@destroy');

// Change blade tag
Blade::setContentTags('<%', '%>');        // for variables and all things Blade
Blade::setEscapedContentTags('<%%', '%%>');   // for escaped data
