<?php
 
class Product extends Eloquent {
 
    protected $table;
	protected $base_name = '_product';

    public function __construct() {
    	// Get store id from session
    	$site = Session::get('store');

    	$this->table = $site->id . $this->base_name;
    }

    public function meta() {
    	return $this->hasMany('ProductMeta');
    }
}
