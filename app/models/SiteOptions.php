<?php
 
class SiteOptions extends Eloquent {
 
    protected $table;
    public $timestamps = false;
    protected $base_name = '_site_options';

    public function __construct() {
    	// Get store id from session
    	$site = Session::get('store');

    	$this->table = $site->id . $this->base_name;
    }
 
}