<?php
 
class ProductMetas extends Eloquent {
 
    protected $table;
    public $timestamps = false;
	protected $base_name = '_product_metas';

    public function __construct() {
    	// Get store id from session
    	$site = Session::get('store');

    	$this->table = $site->id . $this->base_name;
    }

    public function product () {
    	return $this->belongsTo('Product');
    }
}
