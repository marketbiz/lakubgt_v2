<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * Get All Basic Info of Store
	 *
	 *
	 * @since 2.0.0
	 */
	public function getStoreBasicInfo($store_id, $page_name = 'dashboard') {

		$datas = array(
			'page_title' => $page_name,
			'url' => url(),
			'user' => array(
				'name' => Auth::user()->name, 
				'id_user' => Auth::user()->id),
			'store_id' => $store_id
			// 'site_info' => 	DB::table($store_id . '_site_options')->get()
		);

		return $datas;
	}

	/**
	 *
	 *
	 *
	 */
	public function getBaseInfo() {
		$store = new SiteController;

		if (Auth::check()):
			$datas = array(
				'user' => array(
					'name' => Auth::user()->name,
					'id' => Auth::user()->id,
					'email' => Auth::user()->email
				),
				'store' => $store->getStore()
			);
		else:
			$datas = array('user' => null);
		endif;

		return Response::json(array('error' => false, 'baseInfo' => $datas));
	}


	/**
	 * Get all basic front end
	 *
	 *
	 *
	 *
	 * @since 2.0.0
	 */
	public function getFrontendInfo($store_id, $page_name = 'index') {

		$site_infos = DB::table($store_id . '_site_options')->get();
		$site_info = array();

		foreach ($site_infos as $value) {
			// print_r($value);
			$temp = array($value->key => $value->value);
			array_push($site_info, $temp);
		}

		$datas = array(
			'page_title' => $page_name,
			'url' => url('themes'),
			'site_info' => $site_info
		);

		return $datas;
	}

	/**
	 * Get single site info
	 *
	 * @since 2.0.0
	 */
	public function getOption($store_id, $key) {

		$option = DB::table($store_id . '_site_options')->where('key', $key)->first();

		return $option;

	}


	/**
	 *
	 *
	 *
	 *
	 */
	public function getStoreInfo($store_name) {
		$site_info = DB::table('site_list')->where('domain', $store_name)->first();
		return $site_info;
	}


	public function sendMail() {
		Mail::send('emails.notification', array('key' => 'value'), function($message) {
		    $message->from('rizal@marketbiz.net', 'Rizal Syah')->to('yudhi@marketbiz.net', 'Yudhi Satrio')->subject('You made it');

		    print_r($message);
		});
	}
}
