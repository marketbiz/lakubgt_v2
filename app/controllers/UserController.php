<?php

class UserController extends \BaseController {

	/**
	 * Check email exist or not
	 *
	 * 
	 * @return Response
	 */
	public function checkEmail($email) {

		$user = DB::table('users')->where('email', $email)->get();

		return (count($user) != 0) ? true : false;
	}

	/**
	 * Create new user
	 *
	 * Module to create user
	 *
	 * @return Response
	 */
	public function createNewUser($email, $name, $password) {

		$user = new User;

		$user->email = $email;
		$user->name = $name;
		$user->password = Hash::make($password);
		$user->activation_key = Hash::make(rand());

		// Default role for user is 2
		$user->role = 2;

		$user->save();

		if ($user->id) return $user->id; else return 0;
	}

	/**
	 * Validate user when login
	 *
	 *
	 * @return Response
	 * @since 2.0.0
	 */
	public function validateUser() {

		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')), true)) {
		    // return Redirect::to('users/dashboard')->with('message', 'You are now logged in!');
		    $validate = array('error' => false, 'login' => true, 'url' => url('dashboard'));

		    $store = new SiteController;
		    $store->pushStore(Auth::user()->id);
		} else {
		    // return Redirect::to('users/login')
		    //     ->with('message', 'Your username/password combination was incorrect')
		    //     ->withInput();
		    $validate = array('error' => false, 'login' => false);
		}

		// $validate = array('error' => false, 'login' => false);

		return Response::json($validate);
	}


	/**
	 * Get User Actived
	 *
	 *
	 * @since 2.0.0
	 */
	public function getUserActived() {
		$user = Auth::user();

		return Response::json(array('error' => false, 'user' => $user));
	}

	/**
	 * Logout function
	 *
	 * @since 2.0.0
	 */
	public function doLogout() {
		Auth::logout();
		$logout = array( 'error' => 'false', 'url' => url('login'));
		return Response::json($logout);
	}
}