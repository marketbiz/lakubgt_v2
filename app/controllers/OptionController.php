<?php

class OptionController extends \BaseController {

	/**
	 * Default site options
	 *
	 * Save default value of store
	 *
	 *
	 * @since 2.0.0
	 */
	public function defaultSiteOptions($store_id, $options) {

		foreach ($options as $key => $value) {
			$this->saveSiteOptions($store_id, $key, $value);
		}

		return true;
	}

	/**
	 * Save Site Options
	 *
	 * This is module for handle save information of store
	 *
	 * @param $store_id
	 * @param $key
	 * @param $value
	 * @since 2.0.0
	 */
	public function saveSiteOptions($store_id, $key, $value) {

		$instance = new SiteOptions($store_id);

		$instance->key = $key;
		$instance->value = $value;
		$instance->save();

		return $instance;
	}
}