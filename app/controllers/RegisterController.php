<?php

class RegisterController extends \BaseController {

	/**
	 * Display Register Form
	 *
	 * @return Response
	 * @since 2.0.0
	 */
	public function index() {	

		$data = array(
			'page_title' => 'Register',
			'url' => url(),
			'storename' => Session::get('regStoreName'),
			'email' => Session::get('regEmail'),
			'password' => Session::get('regPassword')
		);

		return View::make('register', $data);
	}
	
	/**
	 * Availability Store Name
	 *
	 * Check availability of store name and email
	 *
	 *
	 * @return Response
	 * @since 2.0.0
	 */
	public function availabilityName() {

		extract(Input::all());

		$siteInstance = new SiteController;
		$doCheckName = $siteInstance->checkStoreName($storename);

		$userInstance = new UserController;
		$doCheckEmail = $userInstance->checkEmail($email);

		if ( !$doCheckName && !$doCheckEmail) {
			$availability = array('error' => false, 'availability' => true);

			// Store value in session
			Session::put('regEmail', $email);
			Session::put('regStoreName', $storename);
			Session::put('regPassword', $password);

		} else {
			$availability = array('error' => false, 'availability' => false, 'unavailable' => array('storename' => $doCheckName, 'email' => $doCheckEmail));
		}

		return Response::json($availability);
	}	


	/**
	 * Create new store
	 *
	 *
	 * @since 2.0.0
	 */
	public function postNewStore() {

		// Get all input from sender
		extract(Input::all());

		// Create a new user 
		$user = new UserController;

		$save_user = $user->createNewUser($email, $name, $password);

		// Create a new store
		$store = new SiteController;

		$save_store = $store->store($save_user, $storename);

		if ($save_store != 0) {

			// Create new table for new store
			$this->createTableProducts($save_store);
			$this->createTableCategories($save_store);
			$this->createTableCategoryRelations($save_store);
			$this->createTableProductMetas($save_store);
			$this->createTableSiteOptions($save_store);
			$this->createTableOrders($save_store);
			$this->createTableCustomers($save_store);	
			$this->createTablePages($save_store);
			$this->createTablePageMetas($save_store);

			$store_default_options = array(
				'storename' => $storename,
				// 'store_description' => 
				'admin_email' => $email,
				'tel' => $tel,
				'address' => $address,
				'city' => $city,
				'seo_title' => '',
				'seo_desc' => '',
				'seo_keywords' => '',
				'products_per_page' => 12,
				'template' => 'default17',
				'currency' => 'idr',
				'show_product' => 'latest', // latest | rand
				'upload_folder' => '',
				'logo' => ''
			);

			// Save the information to options
			$options = new OptionController;
			$save_options = $options->defaultSiteOptions($save_store, $store_default_options);

			// Destroy Session
			// Session::flush();
		}

		return Response::json(array('error' => false, 'store' => $store));
		// return Response::json(array('error' => false));
	}

	/**
	 * Create table page
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTablePages($id_store) {

		$tableName = $id_store . '_page';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->integer('author');
			$table->string('title');
			$table->string('slug');
			$table->text('content');
			$table->text('excerpt');
			$table->boolean('status');
			$table->boolean('deleted');
			$table->timestamps();
		});

		return true;
	}

	/**
	 * Create table page meta
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTablePageMetas($id_store) {

		$tableName =  $id_store . '_page_meta';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->integer('page_id');
			$table->string('key');
			$table->string('value');
		});	

		return true;
	}

	/**
	 * Create table product
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTableProducts($id_store) {

		$tableName =  $id_store . '_product';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->integer('author');
			$table->string('title');
			$table->string('slug');
			$table->text('content');
			$table->text('excerpt');
			$table->boolean('status');
			// $table->boolean('deleted');
			$table->softDeletes();
			$table->timestamps();
		});

		return true;
	}

	/**
	 * Create table categories
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTableCategories($id_store) {
		
		$tableName =  $id_store . '_category';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->text('desc');
			$table->integer('parent');
			$table->timestamps();
		});

		return true;
	}

	/**
	 * Create table category relations
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTableCategoryRelations($id_store) {

		$tableName =  $id_store . '_category_relation';

		Schema::create($tableName, function($table) {
			$table->integer('product_id');
			$table->integer('cat_id');
		});

		return true;
	}

	/**
	 * Create table product metas
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTableProductMetas($id_store) {

		$tableName = $id_store . '_product_metas';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')->references('id')->on('product');
			$table->string('key');
			$table->string('value');
		});

		return true;
	}

	/**
	 * Create table site options
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTableSiteOptions($id_store) {

		$tableName = $id_store . '_site_options';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->string('key');
			$table->string('value');
		});

		return true;
	}

	/**
	 * Create table order
	 *
	 * Create new table when new store created
	 * 
	 * @since 2.0.0
	 */
	public function createTableOrders($id_store) {

		$tableName = $id_store . '_orders';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->integer('product_id');
			$table->string('key');
			$table->string('value');
			$table->timestamps();
		});

		return true;
	}

	/**
	 * Create table customers
	 *
	 * Create new table when new store created
	 *
	 * @since 2.0.0
	 */
	public function createTableCustomers($id_store) {

		$tableName = $id_store . '_customers';

		Schema::create($tableName, function($table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('password');
			$table->boolean('status');
			$table->boolean('deleted');
			$table->timestamp('added_on');
		});

		return true;
	}
}
