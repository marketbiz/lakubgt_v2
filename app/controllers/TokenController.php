<?php

class TokenController extends \BaseController {

	/**
	 * Display show api token
	 *
	 * @return Response
	 */
	public function index() {
		
		$token = DB::table('api_token')->get();

	    return Response::json(array(
	        'error' => false,
	        'token' => $token),
	        200
	    );
	}

	/**
	 * Validate token
	 *
	 * @return Response
	 */
	public function getCheck($token) {

		$check_token = DB::table('api_token')->where('token', $token)->get();
		// $check_token = DB::table('api_token')->get();

		return Response::json(array(
			'error' => false,
			'token' => $check_token),
			200
		);
	}
}
