<?php

class OrderController extends \BaseController {

	/**
	 * Show Index 
	 *
	 *
	 * @since 2.0.0
	 */
	public function showIndex() {
		$data = array(
			'page_title' => 'Order',
			'url' => url()
		);

		return View::make('dashboard.order', $data);
	}

	/**
	 *
	 *
	 *
	 */
	// public function 
}