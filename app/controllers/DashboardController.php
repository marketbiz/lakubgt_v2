<?php

class DashboardController extends \BaseController {

	/**
	 * Constructor
	 *
	 * Prepare the dashboard
	 */
	public function __construct() {
		$this->index();
	}

	/**
	 * The Dashboard 
	 *
	 *
	 * @since 2.0.0
	 */
	public function index() {

		$data = array(
			'page_title' => 'Dashboard',
			'url' => url()
		);

		return View::make('dashboard/index', $data);
	}


	/**
	 * Login form
	 *
	 *
	 * @since 2.0.0
	 */
	public function showLogin() {
		$data = array(
			'page_title' => 'Login',
			'url' => url()
		);

		return View::make('login', $data);
	}

	
}