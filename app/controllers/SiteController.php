<?php

class SiteController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Retrieve data using eloquent
		$site = Site::all();

	    return Response::json(array(
	        'error' => false,
	        'site_list' => $site),
	        200
	    );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('add_site');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($user_id, $storename)
	{
		$normalize_storename = str_replace(' ', '-', $storename);

		$site = new Site;
		$site->domain = strtolower($normalize_storename);
		$site->public = 1;
		$site->deleted = 0;
		$site->id_user = $user_id;

		$site->save();

		if ($site->id) return $site->id; else return 0;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $site = DB::table('site_list')->where('id', $id)->get();
		$site = Site::find($id);

	    return Response::json(array(
	        'error' => false,
	        'site_list' => $site),
	        200
	    );
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * Check store name exist or not
	 *
	 * @return Response
	 */
	public function checkStoreName($storename) {

		$site = DB::table('site_list')->where('domain', $storename)->get();
		// $site = DB::table('site_list')->get();
		// $site = Site::where('domain');

		return (count($site) != 0) ? true : false;
	    // return Response::json(array(
	    //     'error' => false,
	    //     'total' => count($site),
	    //     'site_list' => $site)	
	    // );
	}


	/**
	 * Check storename in old database
	 *
	 *
	 * @return Response
	 */
	public function checkExistingStore($storename) {
		
	}


	/**
	 * Push Store to Session
	 *
	 *
	 *
	 * @return Response
	 */
	public function pushStore($user_id) {

		$site = DB::table('site_list')->where('id_user', '=', $user_id)->first();

		Session::put('store', $site);
	}


	/**
	 * Get Store ID
	 *
	 * @return Response
	 */
	public function getStore() {
		// $user_id = Input::get('userId');

		// $site = DB::table('site_list')->where('id_user', '=', $user_id)->first();

		$site = Session::get('store');

		// return Response::json(array( 'error' => false, 'store' => $site));
		return $site;
	}
}
