<?php

class ProductController extends \BaseController {

	/**
	 * Add new product
	 *
	 *
	 * @since 2.0.0
	 */
	public function addProduct() {
		$store = new SiteController;
		$store_info = $store->getStore();

		$data = $this->getStoreBasicInfo($store_info->id, 'Products');

		return View::make('dashboard/new-product', $data);
	}

	/**
	 * Show Product
	 *
	 *
	 * @since 2.0.0
	 */
	public function showProduct() {

		$store = new SiteController;
		$store_info = $store->getStore(Auth::user()->id);

		$data = $this->getStoreBasicInfo($store_info->id, 'Products');

		return View::make('dashboard/index', $data);
	}

	/**
	 * Get Products
	 *
	 *
	 * @since 2.0.0
	 */
	public function index() {

		$store = new SiteController;
		$store_info = $store->getStore(Auth::user()->id);

		$products = DB::table($store_info->id . '_product')->get();

		$temp_array = array();
		foreach ($products as $product) {
			// Get metas
			$metas = $this->getMetaProduct($store_info->id, $product->id);

			// Rearrange the product data
			$array = array('info' => $product, 'metas' => $metas);

			// Push rearrange data to $temp_array
			array_push($temp_array, $array);
		}

		$products = $temp_array;

		return Response::json(array('error' => false, 'products' => $products));
	}


	/**
	 * Get Product
	 *
	 *
	 *
	 * @since 2.0.0
	 */
	public function show($product_id) {

		$store = new SiteController;
		$store_info = $store->getStore(Auth::user()->id);

		$product = DB::table($store_info->id . '_product')->where('id', $product_id)->first();

		$metas = DB::table($store_info->id . '_product_metas')->where('product_id', $product_id)->get();

		$temp_array = array();
		foreach ($metas as $meta) {

			$temp_array[$meta->key] = $meta->value;
		}

		$metas = $temp_array;

		return Response::json(array('error' => false, 'product' => $product, 'metas' => $metas));
	}

	/**
	 * Post New Product
	 *
	 *
	 * @since 2.0.0
	 */
	public function store() {

		// Get all input from sender
		extract(Input::all());

		$store = new SiteController;
		$store_info = $store->getStore();

		$product = new Product;

		$product->title = $title;
		$product->slug = str_replace(' ', '-', strtolower($title));
		$product->content = $content;
		$product->author = Auth::user()->id;
		$product->status = 1;

		$product->save();

		if ($product->id) {
			$this->postMetaProduct($product->id, 'sku', $sku);
			$this->postMetaProduct($product->id, 'reg_price', $reg_price);
			$this->postMetaProduct($product->id, 'sale_price', $sale_price);
			if ($manage_stock) $this->postMetaProduct($product->id, 'manage_stock', $manage_stock);
			$this->postMetaProduct($product->id, 'stock_status', $stock_status);
			$this->postMetaProduct($product->id, 'stock_qty', $stock_qty);
			$this->postMetaProduct($product->id, 'weight', $weight);
			$this->postMetaProduct($product->id, 'dimension', $dimension);
		}

		/*
			'sku'
			'regular_price'
			'sale_price'
			'manage_stock' // Boolean
			'stock_status' // Conditional if manage_stock set to false
			'stock_qty' // Conditional if manage_stock set to true
			'weight(kg)'
			'dimensions(cm)'
			'product_image_1'
			'product_image_2'
			'product_image_3'
			'product_image_4'
		*/

		return Response::json(array(
	        'error' => false,
	        'save' => true,
	        'url' => url('dashboard') . '#/edit-product/' . $product->id),
	        200
	    );
	}

	/**
	 * Edit
	 *
	 * @since 2.0.0
	 */
	public function edit() {

	}

	/**
	 * Destroy
	 *
	 *
	 *
	 * @since 2.0.0
	 */
	public function destroy($id) {

		$product = Product::find($id);
		$product->delete();
		return Response::json(array( 'error' => false, 'deleted' => $product->id));
	}

	/**
	 * Post Meta Product
	 *
	 *
	 * @since 2.0.0
	 */
	public function postMetaProduct($product_id, $key, $value) {

		$instance = new ProductMetas();

		$instance->product_id = $product_id;
		$instance->key = $key;
		$instance->value = $value;

		$instance->save();

		return true;
	}


	/**
	 * Get Meta Product
	 *
	 *
	 * @since 2.0.0
	 */
	public function getMetaProduct($store_id, $product_id) {

		$metas = DB::table($store_id . '_product_metas')->where('product_id', $product_id)->get();

		$temp_array = array();
		foreach ($metas as $meta) {
			$temp_array[$meta->key] = $meta->value;
		}

		$metas = $temp_array;

		return $metas;
	}
}
