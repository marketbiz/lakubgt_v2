<?php

class FrontendController extends \BaseController {

	public $theme;
	public $site_infos;

	public function __construct() {
				
	}

	public function showIndex($store_name) {
		$this->site_infos = $this->getStoreInfo($store_name);

		$data = $this->getFrontendInfo($this->site_infos->id);

		// Get actived template
		$template = $this->getOption($this->site_infos->id, 'template');

		return View::make($template->value . '/index', $data);
	}

	public function getInfos() {

	}
}