<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		// $this->call('TokenTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

		User::create(array(
			'email' => 'yudhi@marketbiz.net',
			'name' => 'Yudhi Satrio',
			'password' => Hash::make('akulupa'),
			'role' => 1,
			'deleted' => 0,
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		));
    }

}

class TokenTableSeeder extends Seeder {

	public function run() {
		DB::table('api_token')->delete();

		Token::create(array (
			'user_id' => 1,
			'token' => Hash::make(uniqid()),
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		));
	}
}
