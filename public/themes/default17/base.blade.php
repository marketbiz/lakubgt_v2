<!-- Stored in default17/base.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- include head -->
	@include('default17.head')	
</head>
<body>
	<!-- include header -->
	@include('default17.header')
	
	<!-- extend main content -->
	@yield('main')
	
	<!-- include footer -->
	@include('default17.footer')	
</body>
</html>