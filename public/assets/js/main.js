(function() {
	'use strict';

	/**
	 * Main script
	 *
	 * Initial define angular module
	 *
	 *
	 */

	 var lakubgt = angular.module('lakubgt',['ngRoute']);
	 // var baseUrl = 'http://isatrio.local/dev-lakubgt/shop-v2/';
	 var baseUrl = 'http://yo.local/dev-lakubgt/shop-v2/';
	 var baseUrlAPI = baseUrl + 'api/v1/';

	fetchData().then(bootstrapApplication);

    function fetchData() {
        var initInjector = angular.injector(["ng"]);
        var $http = initInjector.get("$http");

        return $http.get(baseUrlAPI + 'base-info').then(function(response) {
            lakubgt.constant("baseInfo", response.data);
        }, function(errorResponse) {
            // Handle error case
        });
    }

    function bootstrapApplication() {
        angular.element(document).ready(function() {
            angular.bootstrap(document, ["lakubgt"]);
        });
    }

	lakubgt.config(['$routeProvider',
	    function($routeProvider) {
			$routeProvider.
			when('/', {
				templateUrl: 'templates/dashboard.html',
				controller: 'DashboardCtrl'
			}).
			when('/product', {
				templateUrl: 'templates/products.html',
				controller: 'ProductCtrl'
			}).
			when('/add-product', {
				templateUrl: 'templates/add-product.html',
				controller: 'ProductCtrl'
			}).
			when('/edit-product/:productId', {
				templateUrl: 'templates/edit-product.html',
				controller: 'ProductCtrl'
			}).
			when('/profil', {
				templateUrl: 'templates/profil.html',
				// controller: 'UserCtrl'
			}).
			when('/settings', {
				templateUrl: 'templates/settings.html',
				controller: 'StoreCtrl'
			});
		}
	]);


	// Register Controller
	lakubgt.controller('RegisterCtrl', [ '$scope', '$http', function RegisterCtrl($scope, $http) {

	 	// Execute Register
	 	$scope.doRegister = function() {
	 		var url = baseUrlAPI + 'post-new-store';
	 		var data = $('#form-register').serializeArray();
	 		var postData = {};

	 		_.each(data, function(loop) {
	 			postData[loop.name] = loop.value;
	 		});

	 		$http.post( url, postData).then(
	 			function(response) { // Success
	 				console.log(response);
	 				window.location.replace(baseUrl + 'login');
	 			},
	 			function(response) { // Error
	 				console.log(response);
	 			}
	 		);
	 	};

	}]);

	/**
	 * Login Controller
	 *
	 */
	lakubgt.controller('LoginCtrl',['$scope', '$http', function LoginCtrl($scope, $http) {

		var el = $('#login-form');

		// To show error when login failed
		$scope.alert = true;

		$scope.doLogin = function() {
			var url = baseUrlAPI + 'validate-user';
			var data = $('#login-form').serializeArray();
			var postData = {};

			_.each(data, function(loop) {
	 			postData[loop.name] = loop.value;
	 		});

	 		$http.post(url, postData).then(
				function(response) { // success
					// console.log(response.data);
					if (response.data.login === false) {
						$scope.alert = false;
					} else {
						window.location.replace(response.data.url);
					}
				},
				function(response) { // failed
					console.log(response);
				}
			);
		};

		$scope.closeAlert = function() {
			$scope.alert = true;
		};

	}]);



	lakubgt.controller('DashboardCtrl', ['$scope', '$http', 'baseInfo', function DashboardCtrl($scope, $http, baseInfo) {

		$scope.user = baseInfo.baseInfo.user;
		$scope.store = baseInfo.baseInfo.store;
		$scope.menu = 1;
		// console.log(baseInfo.baseInfo.store);

		$scope.getUser = function() {
			var url = baseUrlAPI + 'user-actived';
			$scope.user = {};

			$http.get(url).then(
				function(response) { // success
					// console.log(response);
					angular.copy(response.data.user, $scope.user);
				},
				function(response) { // failed
					console.log(response);
				}
			);
		};

		$scope.getStore = function(userId) {
			var url = baseUrlAPI + 'get-store';
			$scope.store = {};

			var data = { 'userId' : userId};

			$http.post(url, data).then(
				function(response) { // success
					// console.log(response);
					angular.copy(response.data.store, $scope.store);
				},
				function(response) {
					console.log(response);
				}
			);
		};

		$scope.doLogout = function() {
			var url = baseUrlAPI + 'logout';

			$http.get(url).then(
				function(response) { // Success
					console.log(response.data);
					window.location = response.data.url;
				},
				function(response) {
					console.log(response);
				}
			);
			// console.log('clicked');
		};

		$scope.init = function () {
			$scope.getUser();
		};

		$scope.init();
	}]);

	lakubgt.controller('StoreCtrl', ['$scope', '$http', function StoreCtrl($scope, $http) {

	}]);


	lakubgt.controller('ProductCtrl', ['$scope', '$http', '$routeParams', function ProductCtrl($scope, $http, $routeParams) {

		// Get parameter from
		// $scope.productId = $routeParams.productId;

		$scope.deleted = [0];

		$scope.addNewProduct = function() {
			var url = baseUrlAPI + 'post-new-product';
			var data = $('#add-new-product').serializeArray();
	 		var postData = {};

	 		_.each(data, function(loop) {
	 			postData[loop.name] = loop.value;
	 		});

			$http.post( url, postData ).then(
				function(response) { // success
					// console.log(response);
					if (response.data.save === true) {
						window.location.replace(response.data.url);
					}
				},
				function(response) { // failed
					console.log(response);
				}
			);
			// console.log('test');
		};

		$scope.putProduct = function(productId) {
			var url = baseUrlAPI + 'put-product';
			var data = $('#save-product').serializeArray();
			var postData = {};

			_.each(data, function(loop) {
				postData[loop.name] = loop.value;
			});

			$http.put(url, postData).then(
				function(response) { // Success

				},
				function(response) { // Failed

				}
			);
		}

		$scope.deleteProduct = function(productId) {
			var url = baseUrlAPI + 'delete-product/' + productId;
			// console.log(productId);
			$http.get(url).then(
				function(response) { // Success
					console.log(response);
					$scope.deleted.push(response.data.deleted);
					// console.log($scope.deleted);
				},
				function(response) { // Failed
					console.log(response);
				}
			);
		};

		$scope.checkDeleted = function(id) {
			var check = false;

			_.each($scope.deleted, function(loop) {
				if (loop === id) check = true;
			});

			return check;
		}

		// $scope.init = function() {
		// 	// $scope.getProducts();
		// };

		// $scope.init();
	}]);


	/**
	 * Get Products
	 *
	 *
	 * @since 2.0.0
	 */
	lakubgt.directive('allProducts', function() {
		return {
			restrict: 'A',
			// require: '^DashboardCtrl',
			// transclude: true,
			controller: function($scope, $http) {
				$scope.products = {};

				$scope.getProducts = function() {
					var url = baseUrlAPI + 'get-products';
					$scope.products = {};

					$http.get(url).then(
						function(response) { // success
							$scope.products = response.data.products;
							console.log(response);
						},
						function(response) { // failed
							console.log(response);
						}
					);
				};

				$scope.getProducts();
			}
		}
	});

	lakubgt.directive('getProduct', function() {
		return {
			restrict: 'A',
			controller: function($scope, $http, $routeParams) {
				var url = baseUrlAPI + 'get-product/' + $routeParams.productId;
				$scope.product = {};
				$scope.metas = {};

				$http.get(url).then(
					function(response) { // Success
						$scope.product = response.data.product;
						$scope.metas = response.data.metas;
						console.log(response);
					},
					function(response) { // Failed

					}
				);
			}
		};
	});

	lakubgt.directive('integer', function(){
	    return {
	        require: 'ngModel',
	        link: function(scope, ele, attr, ctrl){
	            ctrl.$parsers.unshift(function(viewValue){
					console.log(viewValue);
	                return parseInt(viewValue, 10);
	            });
	        }
	    };
	});



}());
